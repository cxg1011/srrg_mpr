#pragma once
#include <iostream>
#include <srrg_system_utils/system_utils.h>
#include <srrg_types/cloud_3d.h>
#include <srrg_types/vector_2d.h>
#include <vector>

namespace srrg_mpr {

using srrg_core::Float3Image;
using srrg_core::RawDepthImage;
using srrg_core::Float5Image;
using srrg_core::FloatImage;
using srrg_core::IntImage;
using srrg_core::UnsignedCharImage;
using srrg_core::Cloud3D;

typedef Eigen::Matrix<float, 5, 1> Vector5f;
typedef Eigen::Matrix<float, 5, 2> Matrix5_2f;

// MPRPyramidImageEntry: represents an entry of a MPR Image,
// in terms of both point and derivatives
//  point[0]: intensity
//  point[1]: depth
//  point[2-4]: nx, ny, nz
//  derivatives.col(0): col derivative
//  derivatives.col(1): row derivative
struct MPRPyramidImageEntry {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Vector5f _point;
  Matrix5_2f _derivatives;
};

// MPRPyramidImage: 2D array (an Image) of MPRPyramidImageEntry(s)
// allows typical copy/resize operations
typedef srrg_core::Vector2D<MPRPyramidImageEntry,
                            Eigen::aligned_allocator<MPRPyramidImageEntry> >
    MPRPyramidImage;

// MPRPyramidLevel: contains all the data of a specific pyramid level, i.e.
//  - the image as a MPRPyramidImage
//  - a mask of valid points
//  - camera matrix at this level of pyramid
//  - image size
//  - the corresponding cloud
// the MPRPyramidGenerator takes care of initialize Levels
struct MPRPyramidLevel {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  enum CameraType { Pinhole, Spherical };
  enum ChannelType { Depth = 0x1, Intensity = 0x2, Normals = 0x3 };
  enum ChannelMode { Plain = 0x1, RowDerivative = 0x2, ColumnDerivative = 0x3 };

  MPRPyramidLevel(int rows_ = 0, int cols_ = 0);
  MPRPyramidLevel(const MPRPyramidLevel& other) = default;
  MPRPyramidLevel& operator=(const MPRPyramidLevel& other) = default;

  // resize image and mask
  void resize(int rows_, int cols_);

  // initializes a pyramid level from a triplet
  void initialize(const FloatImage& intensity,
                  const FloatImage& depth,
                  const Float3Image& normals);

  // extracts a channel from a pyramid level.
  cv::Mat getChannel(ChannelType type, ChannelMode mode) const;

  // returns a Float3Image having 9 quadrants showing all channels
  cv::Mat getTiledChannels() const;

  // get the MPRPyramidImageEntry obtained with bilinear interpolation. False if
  // outside
  inline bool getSubPixel(Vector5f& p,
                          Matrix5_2f& d,
                          const Eigen::Vector2f& image_point) const;

  // scale the content of this level into dest, depending on dest sizes
  void scale(MPRPyramidLevel& dest) const;

  UnsignedCharImage _mask;  // invalid pizels are 1
  MPRPyramidImage _image;   // image with all stuff;

  float _min_depth;                  // minimum depth of the cloud
  float _max_depth;                  // max depth of the cloud
  int _rows;                         // num rows of all images the level
  int _cols;                         // num cols of all images
  Eigen::Matrix3f _camera_matrix;    // camera matrix for level
  Eigen::Isometry3f _sensor_offset;  // sensor offset
  Cloud3D _cloud;           // cloud of rich points (point, normal, rgb)
  CameraType _camera_type;  // pinhole or speherical
};

// MPRPyramidLevelVector: Vector to store a pyramid of images
typedef std::vector<MPRPyramidLevel, Eigen::aligned_allocator<MPRPyramidLevel> >
    MPRPyramidLevelVector;

// MPRPyramid: A pyramid is actually a LevelVector
typedef MPRPyramidLevelVector MPRPyramid;
}

#include "mpr_pyramid.hpp"
